This example implements a model based on Michaelis-Menten chemical kinetics.

The model is implemented in the gillespy2 package:
https://github.com/GillesPy2/GillesPy2

Please ensure gillespy2 is installed before running this example.
